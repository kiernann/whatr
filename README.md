
<!-- README.md is generated from README.Rmd. Please edit that file -->

# whatr

<!-- badges: start -->

[![CRAN
status](https://www.r-pkg.org/badges/version/whatr)](https://cran.r-project.org/package=whatr)

<!-- badges: end -->

> I’ll take “Packages” for $400, Alex

> This handy R package helps nerds scrape tons of useful data from the
> fan-made Jeopardy archive.

> What is… what R?

## Overview

### Functions

This package contains functions to scrape the fan-made [J\!
Archive](http://www.j-archive.com/)

  - `whatr_data()` returns a list of *all* a game’s data
  - `whatr_scores()` returns a tidy tibble of a game’s score history
  - `whatr_board()` returns a tibble of a game’s board using 3
    sub-functions:
      - `whatr_categories()`
      - `whatr_clues()`
      - `whatr_answers()`
  - `whatr_players()` returns full player names and bios
  - `whatr_summary()`returns round scores and game stats

All you need is the numeric `game_id` variable found in the J\! Archive
URL

## Installation

``` r
# install.packages("devtools")
devtools::install_github("kiernann/whatr")
```

### Data

``` r
library(whatr)
whatr_data(game_id = 1)
#> $info
#> # A tibble: 1 x 3
#>    game  show date      
#>   <dbl> <dbl> <date>    
#> 1     1  4596 2004-09-06
#> 
#> $players
#> # A tibble: 3 x 6
#>    game first  last     occupation                   city           state 
#>   <dbl> <chr>  <chr>    <chr>                        <chr>          <chr> 
#> 1     1 J.D.   Smith    editor and writer            Washington     D.C.  
#> 2     1 Betsey Casman   appeal and grievance analyst Las Vegas      Nevada
#> 3     1 Ken    Jennings software engineer            Salt Lake City Utah  
#> 
#> $board
#> # A tibble: 60 x 5
#>     game  clue category       question                                                     answer   
#>    <dbl> <dbl> <chr>          <chr>                                                        <chr>    
#>  1     1     1 the old testa… let's all flock to read psalm 95, in which humans are compa… sheep    
#>  2     1     2 the old testa… some say the screech owl in the king james bible is lilith,… adam     
#>  3     1     3 the old testa… hagar, carrying this man's baby, fled into the desert after… abraham  
#>  4     1     6 the old testa… according to this old testament book, this 'swords into plo… isaiah   
#>  5     1     7 the old testa… in genesis, joseph is imprisoned after accusations by the w… potiphar 
#>  6     1     4 sean song      the rap on him is he's sometimes 'puffy'                     sean com…
#>  7     1     5 sean song      once married to madonna                                      sean penn
#>  8     1     8 sean song      he won an oscar for 'the untouchables'                       sean con…
#>  9     1     9 sean song      yoko's boy                                                   sean len…
#> 10     1    10 sean song      android hottie in 'blade runner'                             sean you…
#> # … with 50 more rows
#> 
#> $doubles
#> # A tibble: 3 x 9
#> # Groups:   name [1]
#>    game round  clue double   col   row name  change score
#>   <dbl> <dbl> <dbl> <lgl>  <int> <dbl> <chr>  <dbl> <dbl>
#> 1     1     1    23 TRUE       5     3 Ken     4000 10000
#> 2     1     2    33 TRUE       4     3 Ken    -5000 10000
#> 3     1     2    58 TRUE       3     3 Ken     4800 30000
#> 
#> $scores
#> # A tibble: 180 x 6
#>     game round  clue double name   score
#>    <dbl> <dbl> <dbl> <lgl>  <chr>  <dbl>
#>  1     1     1     1 FALSE  Ken        0
#>  2     1     1     1 FALSE  Betsey     0
#>  3     1     1     1 FALSE  J.D.     200
#>  4     1     1     2 FALSE  Ken      400
#>  5     1     1     2 FALSE  Betsey     0
#>  6     1     1     2 FALSE  J.D.     200
#>  7     1     1     3 FALSE  Ken      400
#>  8     1     1     3 FALSE  Betsey   600
#>  9     1     1     3 FALSE  J.D.     200
#> 10     1     1     4 FALSE  Ken      400
#> # … with 170 more rows
#> 
#> $summary
#> # A tibble: 3 x 6
#>    game name   coryat final right wrong
#>   <dbl> <chr>   <dbl> <dbl> <int> <int>
#> 1     1 Ken     29600 10001    38     3
#> 2     1 Betsey   3200  1399     4     2
#> 3     1 J.D.     5000  3599    10     2
```

## Visuals

``` r
library(whatr)
whatr_plot(game_id = 62)
```

![](man/figures/README-plot-1.png)<!-- -->
