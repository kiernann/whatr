---
output: github_document
---

<!-- README.md is generated from README.Rmd. Please edit that file -->

```{r, echo = FALSE}
knitr::opts_chunk$set(
  collapse  = TRUE,
  message   = FALSE, 
  warning   = FALSE, 
  error     = FALSE,
  fig.width = 10,
  fig.path  = "man/figures/README-",
  comment   = "#>"
)
options(width = 100)
```

# whatr

<!-- badges: start -->

[![CRAN status](https://www.r-pkg.org/badges/version/whatr)](https://cran.r-project.org/package=whatr)

<!-- badges: end -->

> I’ll take “Packages” for $400, Alex

> This handy R package helps nerds scrape tons of useful data from the fan-made Jeopardy archive.

> What is… what R?

## Overview

### Functions

This package contains functions to scrape the fan-made [J\! Archive](http://www.j-archive.com/)

* `whatr_data()` returns a list of _all_ a game's data
* `whatr_scores()` returns a tidy tibble of a game’s score history
* `whatr_board()` returns a tibble of a game's board using 3 sub-functions:
  * `whatr_categories()`
  * `whatr_clues()`
  * `whatr_answers()`
* `whatr_players()` returns full player names and bios
* `whatr_summary()`returns round scores and game stats
    
All you need is the numeric `game_id` variable found in the J\! Archive URL

## Installation

```{r install, eval=FALSE}
# install.packages("devtools")
devtools::install_github("kiernann/whatr")
```

### Data

```{r data}
library(whatr)
whatr_data(game_id = 1)
```

## Visuals

```{r plot}
library(whatr)
whatr_plot(game_id = 62)
```
