#' Scrape Jeopardy game meta data
#'
#' @param game_id A J-Archive! game ID number
#' @importFrom graphics text
#' @importFrom dplyr mutate
#' @importFrom dplyr pull
#' @importFrom dplyr slice
#' @importFrom dplyr select
#' @importFrom dplyr bind_rows
#' @importFrom dplyr arrange
#' @importFrom dplyr group_by
#' @importFrom dplyr ungroup
#' @importFrom dplyr row_number
#' @importFrom dplyr if_else
#' @importFrom dplyr inner_join
#' @importFrom xml2 read_html
#' @importFrom rvest html_node
#' @importFrom rvest html_nodes
#' @importFrom rvest html_text
#' @importFrom rvest html_table
#' @importFrom tibble enframe
#' @importFrom tibble as_tibble
#' @importFrom stringr str_split
#' @importFrom stringr str_remove
#' @importFrom stringr str_extract
#' @importFrom stringr str_remove_all
#' @importFrom stringr str_remove
#' @importFrom stringr str_c
#' @importFrom lubridate as_date
#' @importFrom tidyr gather
#' @importFrom tidyr drop_na
#' @importFrom tidyr separate
#' @importFrom magrittr %>%
#' @export

whatr_categories <- function(game_id) {

  # Create game URL
  url <- str_c("http://www.j-archive.com/showgame.php?game_id=", game_id)

  # Read HTML souce code of page
  page <- read_html(url)

  title <- page %>%
    html_node("head > title:nth-child(1)") %>%
    html_text()

  # Extract the show date from the title
  date <- title %>%
    str_extract("[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$") %>%
    as_date()

  # Extract the show name from the title
  show <- title %>%
    str_extract("(\\d+)") %>%
    as.numeric()

  # Extract the category titles and board location
  categories <- page %>%
    html_nodes("table td.category_name") %>%
    html_text() %>%
    tolower() %>%
    enframe(value = "category", name = NULL) %>%
    mutate(round = c(rep(1, 6), rep(2, 6), 3)) %>%
    mutate(col = c(1:6, 1:6, 1)) %>%
    mutate(category = category %>% str_replace_all("\"", "'")) %>%
    mutate(show = show, date = date, game = game_id) %>%
    select(game, show, date, round, col, category)

  return(categories)
}
